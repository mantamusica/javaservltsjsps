/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Rectangulo;

/**
 *
 * @author Chema
 */
@WebServlet(name = "ServletControlador", urlPatterns = {"/ServletControlador"})
public class ServletControlador extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Procesmos la peticion
        //1.Procesamos los parametros
        String accion = request.getParameter("accion");
        
        //2.Creamos los javabeans
        Rectangulo recRequest = new Rectangulo(3,6);
        Rectangulo recSession = new Rectangulo(4,6);
        Rectangulo recApplication = new Rectangulo(3,9);
        
        if("agregaVariables".equals(accion)){
            //3. Compartimos las variables en el alcance seleccionado
            //alcance request
            request.setAttribute("rectanguloRequest", recRequest);
            //alcance session
            HttpSession session = request.getSession();
            session.setAttribute("rectanguloSession", recSession);
            //alcance application
            ServletContext application = this.getServletContext();
            application.setAttribute("rectanguloApplication", recApplication); 
            
            //agregamos un mensaje
            request.setAttribute("mensaje", "las variables fueron agregadas");
            
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else if ("listarVariables".equals(accion)){
            //4. unicamente redireccionamos ya no agregamos variables
            //y se desplegaran solamente las que sigan disponibles
            //segun el alcance de cada variable
            request.getRequestDispatcher("/WEB-INF/alcanceVariables.jsp").forward(request, response);
        } else {
            //4. Redireccionamos a la pagina de inicio agregando un mensaje
            request.setAttribute("mensaje", "accion no proporcionada o desconocida");
            request.getRequestDispatcher("index.jsp").forward(request, response);
            
            //esta linea no agregaría, envia la informacion al jsp
            //sino es una petición al navegador web
            //response.sendRedirect("index.jsp");
            
        }

        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


}
