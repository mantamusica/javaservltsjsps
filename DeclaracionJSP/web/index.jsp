<%-- 
    para agregar comentarios
--%>
<%!
    //declaramos una variable con su metodo get
    private String Usuario ="Alberto";
    private String getUsuario(){
        return this.Usuario;
    }
    //declaramos un contador de visitas
    private int contadorVisitas = 1;
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Declaraciones</title>
    </head>
    <body>
        <h1>JSP Declaraciones</h1>
        Usuario por medio del atributo: <%= this.Usuario%>
        <br>
        Usuario por medio del metodo: <%= this.getUsuario()%>
        <br>
        Contador de visitas desde que se reinició el servidor:
        <%= this.contadorVisitas++%>
    </body>
</html>
