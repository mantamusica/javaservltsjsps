/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Rectangulo;

/**
 *
 * @author Chema
 */
@WebServlet(name = "ServletControlador", urlPatterns = {"/ServletControlador"})
public class ServletControlador extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //1.En este caso no hay necesidad de procesar parametros
        
        //2.Creamos los javabeans
        Rectangulo rec = new Rectangulo(3,6);
        
        //3.Agregamos las variables en cierto alcance
        request.setAttribute("mensaje", "Saludos desde el servlet");
        
        HttpSession session = request.getSession();
        session.setAttribute("rectangulo", rec);
        
        //4.Redirecciones
        RequestDispatcher rd = request.getRequestDispatcher("vista/desplegarVariables.jsp");
        //se propagan los objetos request y response para que puedan ser usados por el JSP seleccionado
        rd.forward(request, response);
        //ya no se necesita hacer nada más después del redireccionamiento,
        //ya que el flujo continua con el JSP
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


}
